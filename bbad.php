<?php
/**
 * BitBucket AutoDeploy Script
 * @author RaminMT
 **/

##### Global Vars & Consts #####
Define('SAVE_PATH', '/home/netrangn/public_html/dev/learning-git/');

$AllowedIP = array();
$AllowedIP['104.192.143'] = array(0, 255);
$AllowedIP['131.103.20'] = array(160, 192);
$AllowedIP['165.254.145'] = array(0, 64);

################################

if($_SERVER['REQUEST_METHOD'] == 'POST' && evalIP() && evalHeaders()):
    /*$cont = json_encode(getallheaders());
    $cont.= file_get_contents('php://input');
    file_put_contents('text.txt', $cont);*/

///echo '<br />Regular Request!';

    $Request_body = file_get_contents('php://input');
    $Request_body = json_decode($Request_body, true);

//echo '<br />Body Got & Decoded!';

    $Repo_link = $Request_body['repository']['links']['html']['href'];
    $Repo_path = SAVE_PATH.$Request_body['repository']['name'].'-master.zip';
//echo '<br />RepoLink Created: '. $Repo_link;
    dlRepoZip($Repo_link.'/get/master.zip', $Repo_path);

    $zip = new ZipArchive();
	if($zip->open($Repo_path) === true) {
		$zip->extractTo(SAVE_PATH);
		$stat = $zip->statIndex(0);
		$folder = $stat['name'];
		$zip->close();
	} else {
		echo "Unable to extract files. Is the repository name correct?";
		unlink($Repo_path);
		return false;
	}

endif;

##### Functions #####
function evalIP() {
    global $AllowedIP;
    $ServerIP = '';

    if (!empty($_SERVER['HTTP_CLIENT_IP']))
        $ServerIP = $_SERVER['HTTP_CLIENT_IP'];
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ServerIP = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else
        $ServerIP = $_SERVER['REMOTE_ADDR'];

//echo 'Requested Server IP:'. $ServerIP;

    list(,,, $ServerIP_range) = explode('.', $ServerIP);
    $ServerIP_key = str_replace(".{$ServerIP_range}", '', $ServerIP);

//echo '<br />Requested Server IP Key: '. $ServerIP_key;

    if($AllowedIP[$ServerIP_key][0] <= $ServerIP_range && $ServerIP_range <= $AllowedIP[$ServerIP_key][1])
        return true;
//echo '<br />Requested Server IP Validation Failed!!!!';
    return false;
}

function evalHeaders() {
    $Request_header = @getallheaders();
    if($Request_header == '')
        $Request_header = $_SERVER;

//echo '<br />X-Event-Key: '. $Request_header['X-Event-Key'];
    if(!isset($Request_header['X-Event-Key']) && empty($Request_header['X-Event-Key']) && $Request_header['X-Event-Key'] != 'repo:push')
        return false;
//echo '<br />User-Agent: '. $Request_header['User-Agent'];
    if(!isset($Request_header['User-Agent']) && empty($Request_header['User-Agent']) && $Request_header['User-Agent'] != 'Bitbucket-Webhooks/2.0')
        return false;
//echo '<br />Attempt-Number: '. $Request_header['X-Attempt-Number'];
    if(!isset($Request_header['X-Attempt-Number']) && empty($Request_header['X-Attempt-Number']))
        return false;
//echo '<br />X-Hook-UUID: '. $Request_header['X-Hook-UUID'];
    if(!isset($Request_header['X-Hook-UUID']) && empty($Request_header['X-Hook-UUID']) && strlen($Request_header['X-Hook-UUID']) != 36)
        return false;
//echo '<br />X-Request-UUID: '. $Request_header['X-Request-UUID'];
    if(!isset($Request_header['X-Request-UUID']) && empty($Request_header['X-Request-UUID']) && strlen($Request_header['X-Request-UUID']) != 36)
        return false;
    return true;
}

function dlRepoZip($url, $path = false) {
    mkdir(SAVE_PATH, 0755, true);
    if(file_exists($path))
        unlink($path);

	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)");
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);

    if ($path) {
        $out = fopen($path, "wb");
        if ($out == FALSE) {
            throw new Exception("Could not open file `$path` for writing");
        }
        curl_setopt($ch, CURLOPT_FILE, $out);
    } else {
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    }

	/*if(!empty($CONFIG['apiUser'])) {
		curl_setopt($ch, CURLOPT_USERPWD, $CONFIG['apiUser'] . ':' . $CONFIG['apiPassword']);
	}*/

	// Remove to leave curl choose the best version
	//curl_setopt($ch, CURLOPT_SSLVERSION,3); 

	$data = curl_exec($ch);
	
	if(curl_errno($ch) != 0) {
		echo "File transfer error: " . curl_error($ch) . "\n";
	}

	curl_close($ch);

	return $data;
}
?>